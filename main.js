// Task list array
let tasks = [];

// Function to render the tasks
function renderTasks() {
  const taskList = document.getElementById("taskList");
  taskList.innerHTML = "";

  tasks.forEach((task, index) => {
    const listItem = document.createElement("li");
    listItem.className = "flex items-center";
    listItem.innerHTML = `
      <input
        type="checkbox"
        id="task${index}"
        class="mr-2"
        ${task.completed ? "checked" : ""}
      />
      <label
        for="task${index}"
        class="flex-1 ${task.completed ? "completed" : ""}"
      >
        ${task.title}
      </label>
      <button
        class="px-2 py-1 bg-red-500 text-white rounded hover:bg-red-600 focus:outline-none focus:ring-2 focus:ring-red-500"
        onclick="deleteTask(${index})"
      >
        <i class="fas fa-trash"></i>
      </button>
    `;

    taskList.appendChild(listItem);
  });
}

// Function to add a new task
function addTask() {
  const taskInput = document.getElementById("taskInput");
  const taskTitle = taskInput.value.trim();

  if (taskTitle !== "") {
    const newTask = {
      title: taskTitle,
      completed: false,
    };

    tasks.push(newTask);
    taskInput.value = "";
    renderTasks();
  }
}

// Function to delete a task
function deleteTask(index) {
  tasks.splice(index, 1);
  renderTasks();
}

// Event listener for add task button
document.getElementById("addTaskBtn").addEventListener("click", addTask);

// Initial rendering of tasks
renderTasks();
